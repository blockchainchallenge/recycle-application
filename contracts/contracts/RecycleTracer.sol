pragma solidity < 0.6.0;

contract RecycleTracer {
    address public contractOwner;

    address public testAddress;

    uint constant CLIENT_TYPE_RECYCLER = 1;
    uint constant CLIENT_TYPE_RECOLECTOR = 2;
    uint constant CLIENT_TYPE_EMPLOYER = 3;
    uint constant CLIENT_TYPE_RECYCLE_FACTORY = 4;

    uint constant CONTAINER_ORGANIC = 1;
    uint constant CONTAINER_PLASTIC = 2;
    uint constant CONTAINER_GLASS = 3;
    uint constant CONTAINER_PAPER = 4;

    struct RecycledPacket {
        uint _id;
        uint _type;
        address _generator;
        address _owner;
        uint _value;
        uint _reward;
        uint _createdAt;
        address[] _clientTrack;
    }

    struct Client {
        uint _id;
        uint _type;
        uint _load;
        uint[] _generatedRecycledPackagesId;
        uint[] _carryRecycledPackagesId;
    }

    mapping(address => Client) clients;
    address[] clientsAddress;

    RecycledPacket[] recycledPackets;

    constructor() public {
        contractOwner = msg.sender;
    }

    function createClient(
        uint clientType
    ) public {
        address userAddress = msg.sender;

        Client memory client = Client(
            clientsAddress.length,
            clientType,
            0,
            new uint[](0),
            new uint[](0)
        );

        clientsAddress.push(userAddress);
        clients[userAddress] = client;
    }

    function test() public {
        testAddress = msg.sender;
    }

    // Retrive all the client data
    function getClient(
        address clientAddress
    )
        public
        view
        returns (
            uint id,
            uint clientType,
            uint load,
            uint generatedRecycledPackages,
            uint carryRecycledPackages
        )
    {
        id = 0;
        clientType = 0;
        load = 0;
        generatedRecycledPackages = 0;
        carryRecycledPackages = 0;

        if (clients[clientAddress]._type > 0) {
            id = clients[clientAddress]._id;
            clientType = clients[clientAddress]._type;
            load = clients[clientAddress]._load;
            generatedRecycledPackages = clients[clientAddress]._generatedRecycledPackagesId.length;
            carryRecycledPackages = clients[clientAddress]._carryRecycledPackagesId.length;
        }
    }


    // Retrieve the carryed packages by the users
    function getUserCarryPackets(
        address userAddress,
        uint index
    )
        public
        view
        returns (
            uint id,
            uint containerType,
            address generator,
            address owner,
            uint value,
            uint reward,
            uint createdAt,
            bool more
        )
    {
        id = 0;
        containerType = 0;
        generator = address(0);
        owner = address(0);
        value = 0;
        reward = 0;
        more = false;

        if (clients[userAddress]._type > 0) {
            more = clients[userAddress]._carryRecycledPackagesId.length > index;

            if (clients[userAddress]._carryRecycledPackagesId.length >= index) {
                uint packetId = clients[userAddress]._carryRecycledPackagesId[index];

                id = recycledPackets[packetId]._id;
                containerType = recycledPackets[packetId]._type;
                generator = recycledPackets[packetId]._generator;
                owner = recycledPackets[packetId]._owner;
                value = recycledPackets[packetId]._value;
                createdAt = recycledPackets[packetId]._createdAt;
                reward = recycledPackets[packetId]._reward;
            }
        }
    }

    function getUserGeneratedPackets(
        address userAddress,
        uint index
    )
        public
        view
        returns (
            uint id,
            uint containerType,
            address generator,
            address owner,
            uint value,
            uint reward,
            uint createdAt,
            bool more
        )
    {
        id = 0;
        containerType = 0;
        generator = address(0);
        owner = address(0);
        value = 0;
        reward = 0;
        more = false;

        if (clients[userAddress]._type > 0) {
            more = clients[userAddress]._generatedRecycledPackagesId.length > index + 1;

            if (clients[userAddress]._generatedRecycledPackagesId.length >= index) {
                uint packetId = clients[userAddress]._generatedRecycledPackagesId[index];

                id = recycledPackets[packetId]._id;
                containerType = recycledPackets[packetId]._type;
                generator = recycledPackets[packetId]._generator;
                owner = recycledPackets[packetId]._owner;
                value = recycledPackets[packetId]._value;
                createdAt = recycledPackets[packetId]._createdAt;
                reward = recycledPackets[packetId]._reward;
            }
        }
    }

    // This function is called by the machine when the user put all the recycled items
    function createPacket(
        address generatorAddress,
        uint containerType,
        uint packetValue,
        uint reward,
        uint createdAt
    ) public {
        address recolectorAddress = msg.sender;

        RecycledPacket memory recycledPacket = RecycledPacket(
            recycledPackets.length,
            containerType,
            generatorAddress,
            recolectorAddress,
            packetValue,
            reward,
            createdAt,
            new address[](0)
        );

        if (clients[generatorAddress]._type > 0) {
            clients[generatorAddress]._generatedRecycledPackagesId.push(recycledPacket._id);
        }

        if (clients[recolectorAddress]._type > 0) {
            clients[recolectorAddress]._carryRecycledPackagesId.push(recycledPacket._id);
            clients[recolectorAddress]._load = clients[recolectorAddress]._load + packetValue;
        }
        recycledPackets.push(recycledPacket);

        recycledPackets[recycledPackets.length - 1]._clientTrack.push(recolectorAddress);
    }

    // the employer recolect the packages from the recolector
    function recolectRecyclePackets(
        address employerAddress
    ) public {
        address recolectorAddress = msg.sender;
        
        require(
            clients[employerAddress]._type == CLIENT_TYPE_EMPLOYER,
            "Recolect is only allowe by employees"
        );

        require(
            clients[recolectorAddress]._type == CLIENT_TYPE_RECOLECTOR,
            "Only can recolect on recolectors machines"
        );

        clients[employerAddress]._load = clients[recolectorAddress]._load;
        clients[recolectorAddress]._load = 0;

        for (
            uint i = 0;
            i < clients[recolectorAddress]._carryRecycledPackagesId.length;
            ++i
        ) {
            uint packageId = clients[recolectorAddress]._carryRecycledPackagesId[i];

            recycledPackets[packageId]._clientTrack.push(employerAddress);
            clients[employerAddress]._carryRecycledPackagesId.push(packageId);
        }

        clients[recolectorAddress]._carryRecycledPackagesId.length = 0;
    }

    // the employer drop the package to the factory
    // function dropPackages() {}
}