contract("RecyCoin", accounts => {
    const deployerAccount = accounts[0];

    const RecyCointArtifact = artifacts.require("RecyCoin");

    let recyCoin;

    beforeEach(async () => {
        recyCoin = await RecyCointArtifact.new({
            from: deployerAccount,
        });
    });

    it("The contract is deployed successfully", () => {
        assert.ok(recyCoin.address);
    });
});
