const Gpio = require('onoff').Gpio;


class GpioReader {
    constructor(pins) {
        this.leds = [];
        this.buttons = [];

        for (let pin of pins) {

            if (pin.option === 'in') {
                let gpioPin = new Gpio(pin.number, 'in', 'both');
                (() => {
                    let intervalId;

                    gpioPin.watch(() => {
                        if (intervalId) {
                            clearTimeout(intervalId);
                        }

                        intervalId = setTimeout(() => {
                            pin.cb();
                        }, 200)
                    });
                })();

                this.buttons.push(gpioPin);
            }
        }
    }

    clearRegister() {
        if(this.buttons.length) {
            for(let pin of this.buttons) {
                pin.unexport();
            }
        }

        if(this.leds.length) {
            for(let pin of this.leds) {
                pin.unexport();
            }
        }
    }
}

module.exports = GpioReader;