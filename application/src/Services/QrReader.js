const jsQr = require('jsqr');

const FakeProvider = require('./QrProviders/FakeProvider');
const RaspberryPiProvider = require('./QrProviders/RaspberryPiProvider');

module.exports = class QrReader {
    constructor(readProvider) {
        switch(readProvider) {
            case 'raspberry': 
                this.readProvider = new RaspberryPiProvider();
                break;
            default:
                this.readProvider = new FakeProvider();
        }
    }

    async read () {
        try {
            const photo = await this.readProvider.takePhoto();
            const code = jsQr(photo.imageData.data, photo.width, photo.height);

            if(!code) {
                throw Error('No QR scanned');
            }

            return code;
        } catch(error) {
            throw Error('No QR scanned');
        }

    }
}