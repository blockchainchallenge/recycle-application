const Web3 = require('web3');

const Tx = require('ethereumjs-tx').Transaction;

const contract = require('../../../contracts/build/contracts/RecyCoin.json')


class EthereumCoinService  {
    constructor(
        contractAddress,
        urlProvider,
        address,
        privateKey
    ) {
        this.contractAddress = contractAddress;
        this.urlProvider = urlProvider;
        this.address = address;
        this.privateKey = privateKey;

        this.web3 = new Web3(this.urlProvider);
    }

    async createUserReward (userAddress, reward){}

   
    async addAssMinter(address) {
        const contract = await this._getContract(this.contractAddress);
        const encodedData = contract.methods.addMinter(
            address
        ).encodeABI();

        const transaction = await this._sendTransaction(encodedData);
    }

    async mint(address, amount) {
        const contract = await this._getContract(this.contractAddress);
        const encodedData = contract.methods.mint(
            address,
            amount
        ).encodeABI();

        const transaction = await this._sendTransaction(encodedData);
        console.log(transaction);
    }

    async getBalance(address) {
        const contract = await this._getContract(this.contractAddress);
        const balance = await contract.methods.balanceOf(address).call();

        console.log('el balance es');
        console.log(balance);
    }

    async _sendTransaction(encodedABI) {
        const transaction = await this._createTransaction(encodedABI);
        console.log('transaction')
        const signedTransaction = await this._signTransaction(transaction, this.privateKey);
        console.log('signed trans');
        return this._sendSignedTransaction(signedTransaction);
    }

    async _sendSignedTransaction (signedTransaction) {
        return this.web3.eth.sendSignedTransaction(signedTransaction);
    }

    async _createTransaction(
        data,
        address
    ) {
        const userAddress = address || this.address;
        const count = await this._getTransactionCount(userAddress);

        const txData = {
            nonce: count,
            from: userAddress,
            to: this.contractAddress,
            gasLimit: 3000000,
            gasPrice: 0,
            data: data,
        };

        return new Tx(txData);
    }

    async _signTransaction(transaction, privateKey) {
        transaction.sign(Buffer.from(privateKey, "hex"));

        return "0x" + transaction.serialize().toString("hex");
    }

    async _getTransactionCount(address) {
        return this.web3.eth.getTransactionCount(address);
    }

    async _getContract(address) {
        return new this.web3.eth.Contract(contract.abi, address);
    }
}

module.exports = EthereumCoinService;