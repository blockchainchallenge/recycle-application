const QrReader = require('./QrReader');
const GpioReader = require('./GpioReader');
const Pin = require('../Entity/Pin');


module.exports = (
    ethereumService,
    ethereumCoinService
) => {
    const UserTypes = {
        Recicler: 'c',
        Recolector: 'r',
        Employer: 'e',
        Factory: 'f'
    }

    const PacketTypes = {
        Organic: 1,
        Plastic: 2,
        Glass: 3,
        paper: 4
    }

    const packetType = PacketTypes.Organic;

    
    const reader = new QrReader('raspberry');

    let userAddress;
    let userType;
    let totalItems = 0;
    
    const addItemButton = new Pin(4, 'in', () => {
        switch (userType) {
            case UserTypes.Recicler: {
                console.log('Add item to recicler');
                ++totalItems;
                break;
            }

        }
    });
    
    const finishButton = new Pin(17, 'in', async () => {
        switch (userType) {
            case UserTypes.Recicler: {
                console.log(`Send transaction with total amount  ${totalItems} to address ${userAddress}`);
                await ethereumService.createUserPacket(userAddress, packetType, totalItems, totalItems * 2);
                await ethereumCoinService.mint(userAddress, totalItems * 2);
                userAddress = null;
                userType = null;
                totalItems = 0;
    
                readInput();
                break;
            }
            case UserTypes.Employer: {
                await ethereumService.recolectRecycledPackage(userAddress);

                userAddress = null;
                userType = null;
                totalItems = 0;
                readInput();
                break;
            }
        }
    });
    
    const pinReader = new GpioReader([addItemButton, finishButton]);
    
    const readInput = () => {
        try {
            console.log('Read input');
            reader.read()
                .then((value) => {
                    [userType, userAddress] = value.data.split('/')
    
                    console.log(`El user es:${ userType } y el address ${userAddress}`);
                    if(userType) {
                        userAddress = data.address;
                        userType = data.type;
                    }
                })
                .catch(() => {
                    console.log('No valor leido');
                })
                .finally(() => {
                    if (!userAddress) {
                        readInput();
                    }
                });
        } catch (error) {
            console.log(error);
        }
    }
    
    readInput();
    
    process.on('SIGINT', _ => {
        pinReader.clearRegister();
    })
}