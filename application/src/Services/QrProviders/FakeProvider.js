const Photo = require("../../Entity/Photo");



module.exports = class FakeProvider {
    constructor(){};

    async takePhoto () {
        const photoPath = `${__dirname}/../../../assets/fake5.jpg`;

        const photo = new Photo(photoPath);

        return photo.loadImage();
    }
}