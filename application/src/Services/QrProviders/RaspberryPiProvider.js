const PiCamera = require('pi-camera');
const os = require('os');

const Photo = require('./../../Entity/Photo');

class RaspberryPiProvider {
    constructor() {

    }

    async takePhoto () {
        return new Promise(async (success, reject) => {
            const photoPath = `${ os.tmpdir}/pict.jpg`;

            const myCamera = new PiCamera({
                mode: 'photo',
                output: photoPath,
                width: 640,
                height: 640,
                cfx: '128:128',
                nopreview: true,
              });
              
              myCamera.snap()
                .then(async (result) => {
                    const photo = new Photo(photoPath);
                    await photo.loadImage();

                    success(photo)
                }).catch((error) => {
                    reject(error)
                })
        })  
    }
}

module.exports = RaspberryPiProvider;