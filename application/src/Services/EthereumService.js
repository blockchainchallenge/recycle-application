const Web3 = require('web3');

const Tx = require('ethereumjs-tx').Transaction;

const contract = require('../../../contracts/build/contracts/RecycleTracer.json')

const ClientsType = {
    Recycler: 1,
    Recolector: 2,
    Employer: 3,
    Factory: 4
}

class EthereumService  {
    constructor(
        contractAddress,
        urlProvider,
        address,
        privateKey
    ) {
        this.contractAddress = contractAddress;
        this.urlProvider = urlProvider;
        this.address = address;
        this.privateKey = privateKey;

        this.web3 = new Web3(this.urlProvider);
    }

    async createUserReward (userAddress, reward){}

    async createUserPacket(
        userAddress,
        packetType,
        packetValue,
        reward
    ){
        const contract = await this._getContract(this.contractAddress);
        const encodedData = contract.methods.createPacket(
            userAddress,
            packetType,
            packetValue,
            reward,
            Date.now(),
        ).encodeABI();

        const transaction = await this._sendTransaction(encodedData);

        console.log(transaction);

    }

    async recolectRecycledPackage(employerAddress) {
        const contract = await this._getContract(this.contractAddress);
        const encodedData = contract.methods.recolectRecyclePackets(
            employerAddress
        ).encodeABI();

        const transaction = await this._sendTransaction(encodedData);
        console.log(transaction);
    }
    
    async getClientData(address) {
        const clientAddress = address || this.address;
        const contract = await this._getContract(this.contractAddress);

        const client = await contract.methods.getClient(clientAddress).call();
        console.log(client);
    }

    async getUserPacket(address, offset) {
        const contract = await this._getContract(this.contractAddress);

        const packet = await contract.methods.getUserGeneratedPackets(address, offset).call();
        console.log(packet);
    }

    async createClient(type, address, privateKey) {
        const contract = await this._getContract(this.contractAddress);

        const encodedData = contract.methods.createClient(type).encodeABI();

        const transaction = await this._createTransaction(encodedData, address);
        const signedTransaction = await this._signTransaction(transaction, privateKey);

        const receipt = await this._sendSignedTransaction(signedTransaction);
        console.log(receipt);
    }

    async _sendTransaction(encodedABI) {
        const transaction = await this._createTransaction(encodedABI);
        console.log('transaction')
        const signedTransaction = await this._signTransaction(transaction, this.privateKey);
        console.log('signed trans');
        return this._sendSignedTransaction(signedTransaction);
    }

    async _sendSignedTransaction (signedTransaction) {
        return this.web3.eth.sendSignedTransaction(signedTransaction);
    }

    async _createTransaction(
        data,
        address
    ) {
        const userAddress = address || this.address;
        const count = await this._getTransactionCount(userAddress);

        const txData = {
            nonce: count,
            from: userAddress,
            to: this.contractAddress,
            gasLimit: 3000000,
            gasPrice: 0,
            data: data,
        };

        return new Tx(txData);
    }

    async _signTransaction(transaction, privateKey) {
        transaction.sign(Buffer.from(privateKey, "hex"));

        return "0x" + transaction.serialize().toString("hex");
    }

    async _getTransactionCount(address) {
        return this.web3.eth.getTransactionCount(address);
    }

    async _getContract(address) {
        return new this.web3.eth.Contract(contract.abi, address);
    }
}

module.exports = EthereumService;