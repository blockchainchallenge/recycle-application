const bundleService = require('./Services/bundleService');
const EthereumService = require('./Services/EthereumService');
const EthereumCoinService = require('./Services/EthereumCoinService');

const provider = "http://192.168.1.161:7545";

const contractAddress = '0xC22e1F889e69AdB3568b296751957cAAd29529e0';
const coinContractAddress = '0x8252D561afC03a3c5DA68C61B881B0d6Ef2e2905';

const recyclerAddress = '0x0423C15552eC43984F82c135f2AF6992F89A7571';
const recyclerPrivateKey = '2967f76e375b5a9e4cbffe7003146128816b294a6aecb03c98703605c2773129';

const recolectorAddress = '0x24905bD72A0738563cD3fA7297c0f0C0A455Ad31';
const recolectorPrivateKey = 'aff0d4505c55046127fad335bd59628a0483adbf2f02879a03e5959bc083d5f8';

const employerAddress = '0xA41A172EbBB849b1C9ac56ef28d33e8cE6d140f8';
const employerPrivateKey = 'f6dfc8caebf82f00f0f38c2bdf7760655dfa4941b86dde6a4b52e12332b09f95';

const factoryAddress = '0xC75355DB96379e1F2A120D365Bbd38e46165b352';
const factoryPrivateKey = '985eca9c1eb762709387dd79833e5ea9f6c24d65eeb44cb5732c9e46cfb0531c';

const deployerKey = '5063a9fed27b620c6c97e6a578fe337a8d29647992b11c332fff6415d3b0b88a';
const deployerAddress = '0xF3ee9348003102111d2F559A6B3dD5AF306c2561';


// client address 0x0423C15552eC43984F82c135f2AF6992F89A7571 priv 2967f76e375b5a9e4cbffe7003146128816b294a6aecb03c98703605c2773129
// machine address '0x24905bD72A0738563cD3fA7297c0f0C0A455Ad31' priv 'aff0d4505c55046127fad335bd59628a0483adbf2f02879a03e5959bc083d5f8'
// recolector address 0xA41A172EbBB849b1C9ac56ef28d33e8cE6d140f8 priv f6dfc8caebf82f00f0f38c2bdf7760655dfa4941b86dde6a4b52e12332b09f95
// factory address 0xC75355DB96379e1F2A120D365Bbd38e46165b352 priv 985eca9c1eb762709387dd79833e5ea9f6c24d65eeb44cb5732c9e46cfb0531c

const ethereumService = new EthereumService(
    contractAddress, 
    provider, 
    recolectorAddress, 
    recolectorPrivateKey
);

const ethereumCoinService = new EthereumCoinService(
    coinContractAddress, 
    provider, 
    deployerAddress, 
    deployerKey
);

const ClientsType = {
    Recycler: 1,
    Recolector: 2,
    Employer: 3,
    Factory: 4
}

const loadData = async () => {
    // await ethereumService.createClient(ClientsType.Recycler, recyclerAddress, recyclerPrivateKey);
    // await ethereumService.createClient(ClientsType.Recolector, recolectorAddress, recolectorPrivateKey);
    // await ethereumService.createClient(ClientsType.Employer, employerAddress, employerPrivateKey);
    // await ethereumService.createUserPacket(recyclerAddress, 1, 4, 8);
    // await ethereumService.createUserPacket(recyclerAddress, 2, 2, 4);
    // await ethereumService.createUserPacket(recyclerAddress, 3, 3, 6);
    // await ethereumService.createUserPacket(recyclerAddress, 4, 6, 12);

    // await ethereumService.getUserPacket(recyclerAddress, 0);
    //await ethereumService.recolectRecycledPackage(employerAddress);
    //await ethereumCoinService.addAssMinter(recolectorAddress);

    // await ethereumCoinService.mint(recyclerAddress, 30);
    // console.log('Recolector balance');
    // await ethereumCoinService.getBalance(recolectorAddress);
    // console.log('User balance');
    // await ethereumCoinService.getBalance(recyclerAddress);
    // //await ethereumService.addAssMinter(recolectorAddress);
    // console.log('Recicler');
    // await ethereumService.getClientData(recyclerAddress);
    // console.log('Recolector');

    // await ethereumService.getClientData(recolectorAddress);
    // console.log('employer');

    // await ethereumService.getClientData(employerAddress);
    //await ethereumService.getClientData(recyclerAddress)
}

//loadData()

bundleService(
    ethereumService,
    ethereumCoinService
);



