var imageLoader = require('get-image-data');


module.exports = class Photo {
    constructor(
        imagePath
    ) {
        this.imagePath = imagePath;
        this.imageData = 0;
        this.width = 0;
        this.height = 0;
        this.options = 0;
    }

    async loadImage () {
        return new Promise((success, reject) => {
            imageLoader(this.imagePath, (error, info) => {
                if (error) {
                    throw new Error(error);
                }
                this.imageData = info;
                this.height  = info.height;
                this.width = info.width;

                success(this);
            })
        })
    }
}