class Pin {
    constructor(
        number,
        option = 'out',
        cb
    ) {
        this.number = number;
        this.option = option;
        this.cb = cb;
    }
}

module.exports = Pin;